package net.esperonova.plugins.vortaro;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TradukiCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length != 1) return false;

        String language = "en";
        if(sender instanceof Player) {
            var section = VortaroPlugin.userConfig.getConfigurationSection(((Player) sender).getUniqueId().toString());
            if(section != null) {
                var value = section.getString("language");
                if(value != null) language = value;
            }
        }

        var definitions = VortaroPlugin.dictionaries.get(language);

        var definition = definitions.get(args[0]);
        if(definition == null) definition = definitions.get(args[0].toLowerCase());
        if(definition == null && args[0].endsWith("n")) definition = definitions.get(args[0].substring(0, args[0].length() - 1));
        if(definition == null && args[0].endsWith("jn")) definition = definitions.get(args[0].substring(0, args[0].length() - 2));
        if(definition == null && args[0].endsWith("j")) definition = definitions.get(args[0].substring(0, args[0].length() - 1));

        if(definition == null) {
            sender.sendMessage("§cNenio troviĝis.");
        }
        else {
            sender.sendMessage("§a" + definition);
        }

        return true;
    }
}
