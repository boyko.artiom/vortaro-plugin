package net.esperonova.plugins.vortaro;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class VortaroPlugin extends JavaPlugin {
    public static HashMap<String, HashMap<String, String>> dictionaries;
    public static FileConfiguration userConfig;

    public static final String[] LANGUAGES = {"be", "cs", "de", "en", "es", "fr", "hu", "nl", "pl", "pt", "ru", "sk"};

    private static File userConfigFile;

    @Override
    public void onEnable() {
        dictionaries = new HashMap<>();
        readDefinitionFile("espdic.txt", "en");

        for (String language : LANGUAGES) {
            readDefinitionFile("revo-" + language + ".txt", language);
        }

        getLogger().info("inited dictionary");

        getCommand("traduki").setExecutor(new TradukiCommand());
        getCommand("lingvo").setExecutor(new LingvoCommand());

        userConfigFile = new File(getDataFolder(), "userConfig.yml");
        userConfig = YamlConfiguration.loadConfiguration(userConfigFile);
    }

    public static void saveUserConfig() {
        try {
            userConfig.save(userConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readDefinitionFile(String filename, String lang) {
        var definitions = dictionaries.computeIfAbsent(lang, _k -> new HashMap<>());

        var stream = getClass().getClassLoader().getResourceAsStream(filename);
        var reader = new BufferedReader(new InputStreamReader(stream));
        try {
            while(true) {
                String line = reader.readLine();
                if(line == null) break;

                // ignore comments and empty lines
                if(line.length() > 0 && !line.startsWith("#")) {
                    int idx = line.indexOf(" : ");
                    if(idx < 0) idx = line.indexOf(" ; "); // for some reason, one of the lines has a semicolon instead
                    if(idx < 0) throw new RuntimeException("Failed to parse dictionary file, line is: " + line);
                    String term = line.substring(0, idx);
                    definitions.put(term, line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to read dictionary file", e);
        }

    }
}
